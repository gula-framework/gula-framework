<?php


namespace Gula\Framework\Controllers;


use Illuminate\Http\Request;

class Colors extends \App\Http\Controllers\Controller
{
    protected $modelName = \Gula\Framework\Models\Colors::class;

    public function index(Request $request, int $pageNumber = 1)
    {
        $params = [
            'pageNumber' => $pageNumber,
            'modelName' => $this->modelName,
            'page' => $pageNumber,
            'listOrder' => ['name' => 'asc'],
        ];

        return (new ListFilter())->getList($params);
    }

    public function add(Request $request)
    {
        $newRecord = new \Gula\Framework\Models\Colors();
        $newRecord->save();

        return redirect('cms/colors/edit/' . $newRecord->id);
    }

    public function edit(Request $request, int $id)
    {
        $record = (new \Gula\Framework\Models\Colors())->where('id', '=', $id)->first();
        $colors = (new \Gula\Framework\Models\Colors())->where('deleted', '=', false)->get();

        $meta = [
            'title' => 'Kleur',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/colors.png',
        ];

        return view('framework::edit_colors', compact('record', 'meta'));
    }

    public function store(Request $request)
    {
        $post = $request->all();
        unset($post['_token']);

        $record = (new \Gula\Framework\Models\Colors())->where('id', '=', $post['id']);
        $record->update($post);

        return redirect('/cms/colors/list');

    }

}
