<?php


namespace Gula\Framework\Controllers;


use Illuminate\Http\Request;

class DeliveryTimes extends \App\Http\Controllers\Controller
{
    protected $modelName = \Gula\Framework\Models\DeliveryTimes::class;

    public function index(Request $request, int $pageNumber = 1)
    {
        $params = [
            'pageNumber' => $pageNumber,
            'modelName' => $this->modelName,
            'page' => $pageNumber,
            'listOrder' => ['name' => 'asc'],
        ];

        return (new ListFilter())->getList($params);
    }

    public function add(Request $request)
    {
        $newRecord = new \Gula\Framework\Models\DeliveryTimes();
        $newRecord->save();

        return redirect('cms/deliverytimes/edit/' . $newRecord->id);
    }

    public function edit(Request $request, int $id)
    {
        $record = (new \Gula\Framework\Models\DeliveryTimes())->where('id', '=', $id)->first();

        $meta = [
            'title' => 'Bezorgtijd',
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/clock.png',
        ];

        return view('framework::edit_delivery_times', compact('record', 'meta'));
    }

    public function store(Request $request)
    {
        $post = $request->all();
        unset($post['_token']);

        $record = (new \Gula\Framework\Models\DeliveryTimes())->where('id', '=', $post['id']);
        $record->update($post);

        return redirect('/cms/deliverytimes/list');

    }
}
