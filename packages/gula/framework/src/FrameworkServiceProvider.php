<?php

namespace Gula\Framework;

use Illuminate\Support\ServiceProvider;

class FrameworkServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->make('Devdojo\Calculator\CalculatorController');
//        $this->app->make('Gula\Framework\Controllers\ListFilter');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations/');
        $this->loadViewsFrom(__DIR__.'/views', 'framework');

        include __DIR__.'/routes.php';
        foreach (glob(__DIR__ . '/helpers/*.php') as $filename) include_once $filename;

    }
}
