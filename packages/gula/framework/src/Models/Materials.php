<?php


namespace Gula\Framework\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;

class Materials extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $guarded = [];

}
