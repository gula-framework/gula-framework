<?php


namespace Gula\Framework\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;

class ProductsMaterials extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @param Request $request
     * @param int $idProduct
     * @param array $materials
     */
    public function updateProductLinks(Request $request, int $idProduct, array $materials)
    {
        $this->where('id_product', '=', $idProduct)->delete();

        foreach ($materials as $idMaterial => $material){
            $link = new $this;
            $link->id_product = $idProduct;
            $link->id_material = $idMaterial;
            $link->save();
        }
    }

}
