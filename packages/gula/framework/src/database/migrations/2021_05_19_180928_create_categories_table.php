<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->integer('id_parent_category');
            $table->char('name', 50);
            $table->char('slug', 100);
            $table->char('meta_title', 50);
            $table->char('meta_description', 250);
            $table->char('image', 250);
            $table->boolean('active')->default(true);
            $table->boolean('deleted')->default(false);
            $table->integer('sort_order');
            $table->timestamps();
            $table->index(['name', 'active', 'deleted', 'id_parent_category', 'sort_order'], 'basic_index');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
