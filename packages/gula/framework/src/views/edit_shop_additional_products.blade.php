@extends('website-cms::base')

@section('content')
    <div class="meta">
        {{csrf_field()}}
        <input type="hidden" name="model" value="{{$scope['table']}}">

    </div>
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $scope['icon'] }}"/> {{ $scope['title'] }}
                            </div>
                            <div class="col-md-10">
                                Wijzigen / Toevoegen
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/cms/{{$scope['table']}}/store" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$record->id}}">

                            <div class="form-group col-6">
                                <label for="name">Titel:</label>
                                <input type="text" class="form-control" name="name"
                                       value="{{$record->name}}">
                            </div>
                            <div class="form-group col-2">
                                <label for="name">Prijs:</label>
                                <input type="number" name="price" step="0.05" min="1"
                                       value="{{$record->price}}" class="form-control" required>
                            </div>

                            <hr/>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group col-3">
                                        <button type="submit" class="btn btn-success">Opslaan</button>
                                    </div>
                                </div>
                                <div class="col-6"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
@endsection
