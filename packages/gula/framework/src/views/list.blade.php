@extends('framework::base')

@section('content')
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $data['icon'] }}"/>&nbsp;<b>&nbsp;{{ $data['title'] }}</b>
                            </div>
                            <div class="col-md-10">
                                <a href="/cms/{{$data['model']}}/add" class=""
                                   id="linkAdd">
                                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/add.png" class="symbolRow"/>
                                    Regel
                                    Toevoegen
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-borderless table-sm table-hover" id="listData">
                            <thead>
                            <tr>
                                @foreach($data['columns'] as $column)
                                    @if($column['name'] === 'id')
                                        <th style="width: 50px;text-align: right">
                                            {{$column['name']}}
                                        </th>
                                    @else
                                        <th>
                                            {{$column['name']}}
                                        </th>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                @foreach($data['columns'] as $column)
                                    @if($column['filter'] === true)
                                        <td><input type="text" class=""
                                                   id="filter_{{$column['name']}}"
                                                   name="searchColumn[]"
                                                   value="*"/>
                                        </td>
                                    @else
                                        <td></td>
                                    @endif
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['list'] as $entity)
                                <tr onclick="document.location='/cms/{{$data['model']}}/edit/{{$entity->id}}'"
                                    style="cursor: pointer">
                                    @foreach($data['columns'] as $column)
                                        @php
                                            $name = $column['name'];
                                        @endphp
                                        @if($column['name'] === 'id')
                                            <td align="right">{{$entity->$name}}</td>
                                        @else
                                            <td>{{$entity->$name}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
