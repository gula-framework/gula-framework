<div class="tab-pane fade" id="features" role="tabpanel" aria-labelledby="features-tab">
    <div class="row">
        <div class="col-3">
            <br/>
            <div class="form-group col-12">
                <label for="color"><b>Kleuren:</b></label><br/>
                @foreach($colors as $color)
                    <input type="checkbox" name="colors[{{$color->id}}]"
                           class="form-check-input" {{ $color->selected ? 'checked' :''}} >{{$color->name}}<br/>
                @endforeach
            </div>
        </div>
        <div class="col-3">
            <br/>
            <div class="form-group col-12">
                <label for="color"><b>Materialen:</b></label><br/>
                @foreach($materials as $material)
                    <input type="checkbox" name="materials[{{$material->id}}]"
                           class="form-check-input" {{ $material->selected ? 'checked' :''}} >{{$material->name}}<br/>
                @endforeach
            </div>
        </div>
    </div>
</div>
