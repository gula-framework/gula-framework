<div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
    <div class="row">
        <div class="col-6">
            <br/>
            <div class="form-group col-3">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" class="form-control"
                       value="{{$record->slug}}" readonly="readonly">
            </div>
            <div class="form-group col-4">
                <label for="meta_title">Meta Title:</label>
                <input type="text" class="form-control" name="meta_title"
                       value="{{$record->meta_title}}">
            </div>
            <div class="form-group col-12">
                <label for="meta_description">Meta Description</label>
                <textarea name="meta_description" maxlength="200"
                          class="form-control">{{$record->meta_description}}</textarea>
            </div>
        </div>
    </div>
</div>
